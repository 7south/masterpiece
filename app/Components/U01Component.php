<?php
namespace App\Components;

use TypeRocket\Template\Component;

class U01Component extends Component
{
    protected $title = 'Überschrift mit Untertext Komponente';

    /**
     * Admin Fields
     */
    public function fields()
    {
        $form = $this->form();

        $backgroud_color = [
            'Hintergrundfarbe 1' => 'bg-primary',
            'Hintergrundfarbe 2' => 'bg-secondary',
        ];
        $alignment = [
            'Links' => '1',
            'Zentriert' => '2',
            'Rechts' => '3',
        ];
                
        echo $form->row(
            $form->text('Pre_Title')->label('Titel'),
            $form->text('Main_Title')->label('Überschrift')
        );
        echo $form->editor('Description')->label('Beschreibung');
        echo $form->row(
            $form->select('Alignment')->label('Ausrichtung')->setOptions($alignment)->setDefault(1),
            $form->select('Box_Background')->label('Hintergrundfarbe')->setOptions($backgroud_color)->setDefault(1)
        );
        echo $form->row(
            $form->toggle('Padding_Above')->label('Abstand oben'),
            $form->toggle('Padding_Below')->label('Abstand unten')
        );
    }

    /**
     * Render
     *
     * @var array $data component fields
     * @var array $info name, item_id, model, first_item, last_item, component_id, hash
     */
    public function render(array $data, array $info)
    {   
        include 'functions.php';
        ?>
        <div class="builder-content <?php if(isset($data['padding_above']) && $data['padding_above'] != 0){ echo 'pt'; } ?>  <?php if(isset($data['padding_below']) && $data['padding_below'] != 0){ echo 'pb'; } ?> <?php data($data,'box_background') ?>">
            <div id="u01">
                <div class="container">
                    <?php 
                    if($data['alignment'] == '1'){
                    ?>
                    <div class="row">
                        <div class="col-12 col-md-10 text-align-left">
                            <label><?php data($data,'pre_title') ?></label>
                            <h2><?php data($data,'main_title') ?></h2>
                            <p><?php data($data,'description') ?></p>
                        </div>
                    </div>
                    <?php 
                    }
                    if($data['alignment'] == '2'){
                    ?>
                    <div class="row justify-center">
                        <div class="col-12 col-md-10">
                        <label><?php data($data,'pre_title') ?></label>
                            <h2><?php data($data,'main_title') ?></h2>
                            <p><?php data($data,'description') ?></p>
                        </div>
                    </div>
                    <?php 
                    }
                    if($data['alignment'] == '3'){
                    ?>
                    <div class="row justify-end">
                        <div class="col-12 col-md-10">
                            <label><?php data($data,'pre_title') ?></label>
                            <h2><?php data($data,'main_title') ?></h2>
                            <p><?php data($data,'description') ?></p>
                        </div>
                    </div>
                    <?php 
                    }
                    ?>
                </div>
            </div>
        </div>
        <?php
    }
}