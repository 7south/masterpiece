<?php

/*

Rquired Plugins

*/

/**
 * Setup error level
 */
add_action( 'init', function() {
	error_reporting( E_ERROR | E_PARSE );
} );

require_once get_template_directory() . '/inc/class-tgm-plugin-activation.php';
add_action('tgmpa_register', 'my_theme_register_required_plugins');
function my_theme_register_required_plugins()
{
	$plugins = array(
		// This is an example of how to include a plugin bundled with a theme.
		array(
			'name'               => 'Typerocket Pro', // The plugin name.
			'slug'               => 'typerocket', // The plugin slug (typically the folder name).
			'source'             => get_template_directory() . '/inc/plugins/typerocket.zip', // The plugin source.
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			'force_activation'   => true, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		)

	);
	tgmpa($plugins);
}

/*

Registers a stylesheet.

*/

function wpdocs_register_plugin_styles()
{
	wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.css');
	wp_enqueue_style('bootstrap');
	wp_register_style('style', get_template_directory_uri() . '/css/style.css');
	wp_enqueue_style('style');
	wp_register_style('font', get_template_directory_uri() . '/css/fonts.css');
	wp_enqueue_style('font');
	wp_register_script('jquery', get_template_directory_uri() . '/js/jquery-3.5.1.min.js');
	wp_enqueue_script('jquery');
	wp_register_script('bootstrapjs', get_template_directory_uri() . '/js/bootstrap.min.js');
	wp_enqueue_script('bootstrapjs');
	wp_register_script('jquery.sticky', get_template_directory_uri() . '/js/jquery.sticky.js');
	wp_enqueue_script('jquery.sticky');
	wp_register_script('functions', get_template_directory_uri() . '/js/functions.js');
	wp_enqueue_script('functions');
	wp_register_script('aos', get_template_directory_uri() . '/js/aos.js');
	wp_enqueue_script('aos');
	wp_register_script('base', get_template_directory_uri() . '/js/base.js');
	wp_enqueue_script('base');
	wp_register_style('aoscss', get_template_directory_uri() . '/css/aos.css');
	wp_enqueue_style('aoscss');
	wp_register_style('base', get_template_directory_uri() . '/css/base.css');
	wp_enqueue_style('base');
	wp_register_style('slick', get_template_directory_uri() . '/css/slick.css');
	wp_enqueue_style('slick');
	wp_register_style('slick-theme', get_template_directory_uri() . '/css/slick-theme.css');
	wp_enqueue_style('slick-theme');
	wp_register_script('slick', get_template_directory_uri() . '/js/slick.js');
	wp_enqueue_script('slick');
}
function wpdocs_admin_register(){

	wp_register_script('functions', get_template_directory_uri() . '/js/functions.js');
	wp_enqueue_script('functions');
	wp_register_style('admin-css', get_template_directory_uri() . '/css/admin.css');
	wp_enqueue_style('admin-css');

}
add_action('admin_enqueue_scripts','wpdocs_admin_register');

/*
Register Menus

*/

add_action('wp_enqueue_scripts', 'wpdocs_register_plugin_styles');

if (!function_exists('mytheme_register_nav_menu')) {

	function mytheme_register_nav_menu()
	{
		register_nav_menus(array(
			'primary_menu' => __('Primary Menu', 'text_domain'),
			'footer_menu'  => __('Footer Menu', 'text_domain'),
			'left_menu'  => __('Left Menu', 'text_domain'),
			'right_menu'  => __('Right Menu', 'text_domain'),
			'topbar_menu'  => __('Topbar Menu', 'text_domain'),
		));
	}
	add_action('after_setup_theme', 'mytheme_register_nav_menu', 0);
}
	
$category = tr_taxonomy('category');

$faqs = tr_post_type('Faq', 'Faqs')
		->setTitlePlaceholder('Title')
		->setIcon('book')
        ->setEditorForm(function(){
			$form = tr_form();
			echo $form->text('question')->label('Question');
			echo $form->editor('answare')->label('Answare');
		})
		->setArgument('supports',['title'])
		->apply($category);

if (!function_exists('cleanSrc')) {
	function cleanSrc($id){
		$imageId = $id;
		if($imageId){
			return wp_get_attachment_image_src($imageId,'full')[0];
		}
	}
	}

if (!function_exists('data')) {
	function data( $data ,$value){
	if(isset($data[$value])){
		echo $data[$value];
	}else{
		echo '';
	}
	}
}


if (!function_exists('background')) {
	function background( $data ,$value ){
	if(isset($data[$value])){
		$image = wp_get_attachment_image_src($data[$value],'full')[0];
		echo 'style="background-image:url(\' '.$image.' \')"';
	}else{
		echo '';
	}
	}
}

/**
 * Add a sidebar.
 */

function wpdocs_theme_slug_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Footer 1', 'otec' ),
        'id'            => 'footer-1',
        'description'   => __( 'Widgets in Footer.', 'otec' ),
        'before_title'  => '<h5 class="footer-widget-title">',
        'after_title'   => '</h5>',
    ) );

	register_sidebar( array(
        'name'          => __( 'Footer 2', 'otec' ),
        'id'            => 'footer-2',
        'description'   => __( 'Widgets in Footer.', 'otec' ),
        'before_title'  => '<h5 class="footer-widget-title">',
        'after_title'   => '</h5>',
    ) );

	register_sidebar( array(
        'name'          => __( 'Footer 3', 'otec' ),
        'id'            => 'footer-3',
        'description'   => __( 'Widgets in Footer.', 'otec' ),
        'before_title'  => '<h5 class="footer-widget-title">',
        'after_title'   => '</h5>',
    ) );

	register_sidebar( array(
        'name'          => __( 'Footer 4', 'otec' ),
        'id'            => 'footer-4',
        'description'   => __( 'Widgets in Footer.', 'otec' ),
        'before_title'  => '<h5 class="footer-widget-title">',
        'after_title'   => '</h5>',
    ) );
}
add_action( 'widgets_init', 'wpdocs_theme_slug_widgets_init' );
function the_breadcrumb() {

    $sep = ' / ';

    if (!is_front_page()) {
	
	// Start the breadcrumb with a link to your homepage
        echo '<div class="breadcrumbs">';
        echo '<a href="';
        echo get_option('home');
        echo '">';
        bloginfo('name');
        echo '</a>' . $sep;
	
	// Check if the current page is a category, an archive or a single page. If so show the category or archive name.
        if (is_category() || is_single() ){
            the_category('title_li=');
        } elseif (is_archive() || is_single()){
            if ( is_day() ) {
                printf( __( '%s', 'text_domain' ), get_the_date() );
            } elseif ( is_month() ) {
                printf( __( '%s', 'text_domain' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'text_domain' ) ) );
            } elseif ( is_year() ) {
                printf( __( '%s', 'text_domain' ), get_the_date( _x( 'Y', 'yearly archives date format', 'text_domain' ) ) );
            } else {
                _e( 'Blog Archives', 'text_domain' );
            }
        }
	
	// If the current page is a single post, show its title with the separator
        if (is_single()) {
            echo $sep;
            the_title();
        }
	
	// If the current page is a static page, show its title.
        if (is_page()) {
            echo the_title();
        }
	
	// if you have a static page assigned to be you posts list page. It will find the title of the static page and display it. i.e Home >> Blog
        if (is_home()){
            global $post;
            $page_for_posts_id = get_option('page_for_posts');
            if ( $page_for_posts_id ) { 
                $post = get_page($page_for_posts_id);
                setup_postdata($post);
                the_title();
                rewind_posts();
            }
        }

        echo '</div>';
    }
}