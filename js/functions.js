jQuery(function ($) { 
    var test = $(".test");
    for (let i = 0; i < test.length; i++) {
        if($(test[i]).is(':checked')){
            if($(test[i]).val() == 2 || $(test[i]).val() == 5 || $(test[i]).val() == 6 || $(test[i]).val() == 7){
                $('#mp_header_selector').css("display", "block");
            }else{
                $('#mp_header_selector').css("display", "none");
            }
        }
    }
    $('.test').click(function(){
        if($(this).val() == 2 || $(this).val() == 5 || $(this).val() == 6 || $(this).val() == 7){
            $('#mp_header_selector').css("display", "block");
        }else{
            $('#mp_header_selector').css("display", "none");
        }
    })
})