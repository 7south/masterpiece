<?php
namespace App\Components;

use TypeRocket\Template\Component;

class H01Component extends Component
{
    protected $title = 'Hero mit Kontaktinfo Komponente';

    /**
     * Admin Fields
     */
    public function fields()
    {
        $form = $this->form();

        $color = [
            'Hauptfarbe'=> 'principal_bg_primary',
            'Sekundäre Farbe'=> 'principal_bg_secondary',
            'Dritte Farbe'=> 'principal_bg_Third',
        ];
        echo $form->row(
            $form->image('Hero_Image')->label('Titelbild'),
            $form->text('Text_Left')->label('Vertikaler Text links'),
            $form->text('Hero_Title')->label('Überschrift'),
            $form->select('Box_Background')->label('Hintergrundfarbe der Box')->setOptions($color)->setDefault(1)
        );
        echo $form->editor('Second_Column_Text')->label('Text der 1. Spalte');
        echo $form->editor('Third_Column_Text')->label('Text der 2. Spalte');
        
    }

    /**
     * Render
     *
     * @var array $data component fields
     * @var array $info name, item_id, model, first_item, last_item, component_id, hash
     */
    public function render(array $data, array $info)
    {   
        include 'functions.php';
        ?>
        <div class="builder-content">
            <div class="container-fluid">
                <div id="h01">
                    <div class="row">
                        <div class="img-holder">
                            <img class="img-fluid" src="<?php echo cleanSrc($data['hero_image']) ?>" alt="o-tec">
                        </div>
                    </div>
                    <div class="container">
                        <div class="row floatDiv <?php data($data,'box_background') ?>">
                            <div class="col-12 col-sm-12 col-md-1 vertical">
                                <p><?php data($data,'text_left') ?></p>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4">
                                <h1><?php data($data,'hero_title') ?></h1>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3">
                                <p>
                                <?php data($data,'second_column_text') ?>
                                </p>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3">
                                <p><?php data($data,'third_column_text') ?></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <?php
    }
}