<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Component Registry
    |--------------------------------------------------------------------------
    */
    'registry' => [
        'b01' => \App\Components\B01Component::class,
        'b02' => \App\Components\B02Component::class,
        'b03' => \App\Components\B03Component::class,
        'f01' => \App\Components\F01Component::class,
        'c01' => \App\Components\C01Component::class,
        'c02' => \App\Components\C02Component::class,
        'h01' => \App\Components\H01Component::class,
        'h02' => \App\Components\H02Component::class,
        'h03' => \App\Components\H03Component::class,
        'h04' => \App\Components\H04Component::class,
        'k01' => \App\Components\K01Component::class,
        'k02' => \App\Components\K02Component::class,
        'l01' => \App\Components\L01Component::class,
        'l02' => \App\Components\L02Component::class,
        'u01' => \App\Components\U01Component::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Builder
    |--------------------------------------------------------------------------
    |
    | List of components you want included for the builder group.
    |
    */
    'builder' => [
        'b01','b02','b03','c01','c02','f01','h01','h02','h03','h04','k01','k02','l01','l02','u01'
    ]
];