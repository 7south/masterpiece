<!-- TOPBAR -->
<?php  require_once 'topbar/topbar_'.tr_option_field('tr_theme_options.topbarformat').'.php'; ?>

<header class='header7<?php if(tr_option_field('tr_theme_options.sticky_header')){echo ' sticky'; } ?>'>
    
    <?php if(tr_option_field('tr_theme_options.grid') == 1){ ?>
    <div class="container">
    <?php } ?>

        <div class="navigation">

            <div class="logo">
            <a href="/"><img src="<?php echo cleanSrc(tr_option_field('tr_theme_options.logo')); ?>" alt="logo-otec"></a>
            </div>

            <div id="menu">
                <ul class="menu-mobile">
                    <div class="contact-block">
                        <p><?php echo tr_option_field('tr_theme_options.email'); ?></p>
                    </div>
                    <li class='button'><a href="<?php echo tr_option_field('tr_theme_options.cta_button_url'); ?>"><?php echo tr_option_field('tr_theme_options.cta_button_text'); ?></a></li>
                </ul>
            </div>

        </div>

    <?php if(tr_option_field('tr_theme_options.grid') == 1){ ?>
    </div>
    <?php } ?>
    
</header>