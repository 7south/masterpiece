<?php
namespace App\Components;

use TypeRocket\Template\Component;

class K02Component extends Component
{
    protected $title = 'Kontakt mit Karte Komponente';

    /**
     * Admin Fields
     */
    public function fields()
    {
        $form = $this->form();

        $backgroud_color = [
            'Hintergrundfarbe 1' => 'bg-primary',
            'Hintergrundfarbe 2' => 'bg-secondary',
        ];
        echo $form->text('Main_Title')->label('Überschrift');
        echo $form->editor('Descript')->label('Beschreibung');
        echo $form->row(
            $form->toggle('Padding_Above')->label('Abstand oben'),
            $form->toggle('Padding_Below')->label('Abstand unten'),
            $form->select('Box_Background')->label('Hintergrundfarbe')->setOptions($backgroud_color)->setDefault(1)
        );
    }

    /**
     * Render
     *
     * @var array $data component fields
     * @var array $info name, item_id, model, first_item, last_item, component_id, hash
     */
    public function render(array $data, array $info)
    {   
        include 'functions.php';
        ?>
<div class="builder-content <?php if(isset($data['padding_above']) && $data['padding_above'] != 0){ echo 'pt'; } ?>  <?php if(isset($data['padding_below']) && $data['padding_below'] != 0){ echo 'pb'; } ?> <?php data($data,'box_background') ?>">
    <div class="container">
        <div id="k02" sty>
            <div class="row">
                <div class="col-12 col-sm-12 col-md-4 info">
                    <h2><?php data($data,'main_title') ?></h2>
                    <?php data($data,'descript') ?>
                </div>
                <?php foreach(tr_option_field('tr_theme_options.branch') as $address){ ?>
                <div class="col-12 col-sm-12 col-md-4">
                    <div class="card">
                        <img class="card-img-top" src="<?php echo cleanSrc($address['map']) ?>">
                        <div class="card-body">
                            <h2 class="card-title"><?php data($address,'title') ?></h2>
                            <span><?php data($address,'street') ?></span><br>
                            <span><?php data($address,'cp') ?> <?php data($address,'city') ?></span><br>
                            <span>Telefon: <?php data($address,'tel') ?></span><br>
                            <?php if(isset($address['buttons']) && $address['buttons'] == '1'){ ?>
                                <a href="<?php data($address,'button_url') ?>" class="btn btn-link"><?php data($address,'button_text') ?></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
        <?php
    }
}