<?php 
if (!function_exists('cleanSrc')) {
    function cleanSrc($id){
      $imageId = $id;
      if($imageId){
          return wp_get_attachment_image_src($imageId,'full')[0];
      }
    }
  }

if (!function_exists('data')) {
  function data( $data ,$value){
    if(isset($data[$value])){
      echo $data[$value];
    }else{
      echo '';
    }
  }
}


if (!function_exists('background')) {
  function background( $data ,$value ){
    if(isset($data[$value])){
      $image = wp_get_attachment_image_src($data[$value],'full')[0];
      echo 'style="background-image:url(\' '.$image.' \')"';
    }else{
      echo '';
    }
  }
}