<?php
namespace App\Components;

use TypeRocket\Template\Component;

class F01Component extends Component
{
    protected $title = 'Faqs Komponente';

    /**
     * Admin Fields
     */
    public function fields()
    {
        $form = $this->form();

        $backgroud_color = [
            'Hintergrundfarbe 1' => 'bg-primary',
            'Hintergrundfarbe 2' => 'bg-secondary',
        ];
        $color = [
            'Hauptfarbe'=> 'principal_bg_primary',
            'Sekundäre Farbe'=> 'principal_bg_secondary',
            'Dritte Farbe'=> 'principal_bg_Third',
        ];
        $terms = get_terms( array( 
            'taxonomy' => 'category',
            'parent'   => 0
        ) );
        $option = [];
        foreach ($terms as $t) {
            $option[$t->name] = $t->name;
        }
        echo $form->row(
            $form->text('Title')->label('Überschrift'),  
            $form->select('Category')->label('Kategorie')->setOptions($option)
        );
        echo $form->editor('description')->label('Beschreibung');
        echo $form->row(
            $form->toggle('Padding_Above')->label('Abstand oben'),
            $form->toggle('Padding_Below')->label('Abstand unten'),
            $form->select('Accent_Color')->label('Farbe der Box bei Hover')->setOptions($color)->setDefault(1),
            $form->select('Box_Background')->label('Hintergrundfarbe')->setOptions($backgroud_color)->setDefault(1)
        );
    }

    /**
     * Render
     *
     * @var array $data component fields
     * @var array $info name, item_id, model, first_item, last_item, component_id, hash
     */
    public function render(array $data, array $info)
    {   
        include 'functions.php';
        ?>
        <div class="builder-content <?php if(isset($data['padding_above']) && $data['padding_above'] != 0){ echo 'pt'; } ?>  <?php if(isset($data['padding_below']) && $data['padding_below'] != 0){ echo 'pb'; } ?> <?php data($data,'box_background') ?>">
            <div class="container-fluid p-none">
                <div id="f01">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4">
                                <h2><?php data($data,'title') ?></h2>
                                <p><?php data($data,'description') ?></p>
                            </div>
                            <div class="col-12 col-sm-12 col-md-8">
                            <div id="accordion" class="faqs-questions">
                                <?php                     
                                $args   =   array(
                                        'post_type'         =>  'faq',
                                        'post_status'       =>  'publish',
                                        'posts_per_page' => 9999,
                                        'tax_query' => array(
                                                array(
                                                    'taxonomy' => 'category',
                                                    'field' => 'slug',
                                                    'terms' => $data['category']
                                                )));
                                $postslist = new \WP_Query( $args );
                                global $post;
                                if ( $postslist->have_posts() ) :
                                    while ( $postslist->have_posts() ) : $postslist->the_post();                
                                ?>
                                <div class="card">
                                    <div class="card-header" id="headingOne-<?php echo the_ID(); ?>">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link " data-toggle="collapse" data-target="#collapseOne-<?php echo the_ID(); ?>" aria-expanded="false" aria-controls="collapseOne-<?php echo the_ID(); ?>">
                                                <span><?php echo trim(tr_field('question'));?></span>
                                                <svg class="svg-inline--fa fa-angle-down fa-w-10 rotate-icon" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" data-fa-i2svg="">
                                                    <path fill="currentColor" d="M143 352.3L7 216.3c-9.4-9.4-9.4-24.6 0-33.9l22.6-22.6c9.4-9.4 24.6-9.4 33.9 0l96.4 96.4 96.4-96.4c9.4-9.4 24.6-9.4 33.9 0l22.6 22.6c9.4 9.4 9.4 24.6 0 33.9l-136 136c-9.2 9.4-24.4 9.4-33.8 0z"></path>
                                                </svg>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseOne-<?php echo the_ID(); ?>" class="collapse" aria-labelledby="headingOne-<?php echo the_ID(); ?>" data-parent="#accordion">
                                        <div class="card-body">
                                        <?php 
                                        echo tr_field('answare');
                                        ?>
                                        </div>
                                    </div>
                                </div>
                                <?php 
                                    endwhile;
                                endif;
                            ?>
                            </div>      
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <?php
    }
}