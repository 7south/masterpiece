jQuery(function() {
    jQuery('.green').mouseenter(function(){
        jQuery(this).find('.dashicons').css('color', '#47ae74')
        jQuery(this).find('h2').css('color', '#47ae74')
        jQuery(this).find('a').css('color', '#47ae74') 
    })

    jQuery('.green').mouseleave(function(){
        jQuery(this).find('.dashicons').css('color', '#3b89c6')
        jQuery(this).find('h2').css('color', '#3b89c6')
        jQuery(this).find('a').css('color', '#3b89c6') 
    })
    $('.slider-master').slick({
        slidesToShow: 1,
        dots: true,
        centerMode: true,
        focusOnSelect: true
      });


    jQuery('.green-card').mouseenter(function(){
        jQuery(this).find('h2').css('color', '#47ae74')
        jQuery(this).find('a').css('color', '#47ae74') 
    })

    jQuery('.green-card').mouseleave(function(){
        jQuery(this).find('h2').css('color', '#232323')
        jQuery(this).find('a').css('color', '#232323') 
    })

    jQuery('.blue').mouseenter(function(){
        jQuery(this).find('h2').css('color', '#47ae74')
        jQuery(this).find('a').css('color', '#47ae74') 
    })

    jQuery('.blue').mouseleave(function(){
        jQuery(this).find('h2').css('color', '#232323')
        jQuery(this).find('a').css('color', '#3b89c6') 
    })
});