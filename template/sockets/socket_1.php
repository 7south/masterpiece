<div class="page-socket" id="socket1">
    <div class="container-socket container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-6">
                <p><?php echo tr_option_field('tr_theme_options.copyright'); ?></p>
            </div>
            <div class="col-12 col-sm-12 col-md-6 rightSocket">
                <div class="menu">
                <?php 
                    wp_nav_menu( array(
                        'menu_class'     => 'menu', // Do not fall back to first non-empty menu.
                        'theme_location' => 'footer_menu',
                        'fallback_cb'    => false// Do not fall back to wp_page_menu()
                    ) );
                ?>
                </div>
            </div>
        </div>
    </div>
</div>