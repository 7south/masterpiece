<?php
namespace App\Components;

use TypeRocket\Template\Component;

class K01Component extends Component
{
    protected $title = 'Kontankt mit Formular Komponente';

    /**
     * Admin Fields
     */
    public function fields()
    {
        $form = $this->form();

        $backgroud_color = [
            'Hintergrundfarbe 1' => 'bg-primary',
            'Hintergrundfarbe 2' => 'bg-secondary',
        ];
        echo $form->row(
            $form->image('Hero_Image')->label('Titelbild'),
            $form->text('Main_Title')->label('Überschrift'),
            $form->select('Box_Background')->label('Hintergrundfarbe')->setOptions($backgroud_color)->setDefault(1)
        );
        echo $form->editor('Description')->label('Beschreibung');
        echo $form->row(
            $form->text('Form_Title')->label('Titel des Formulars'),
            $form->text('Form_Shortcode')->label('Shortcode für das Formular')
        );
    }

    /**
     * Render
     *
     * @var array $data component fields
     * @var array $info name, item_id, model, first_item, last_item, component_id, hash
     */
    public function render(array $data, array $info)
    {   
        include 'functions.php';
        ?>
      <div class="builder-content">
        <div class="container-fluid p-none background-size <?php if(isset($data['padding_above']) && $data['padding_above'] != 0){ echo 'pt'; } ?>  <?php if(isset($data['padding_below']) && $data['padding_below'] != 0){ echo 'pb'; } ?> <?php data($data,'box_background') ?>"" style="background-image: url('<?php echo cleanSrc($data['hero_image']) ?>')">
            <div id="k01">
                <div class="col-12 col-sm-12 col-md-12 p-none">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-5 info">
                                <h2><?php data($data,'main_title') ?></h2>
                                <?php data($data,'description') ?>
                            </div>   
                            <div class="col-12 col-sm-12 col-md-7 form">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title"><?php data($data,'form_title') ?></h4>
                                            <?php 
                                            echo do_shortcode($data['form_shortcode']);
                                            ?>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <?php
    }
}