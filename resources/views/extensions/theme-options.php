<h1>Theme Options</h1>
<?php
/** @var \TypeRocket\Elements\Form $form */
echo $form->useRest()->open();
// General
$general = $form->fieldset('General Info', 'Details about your site.', [
    $form->row()->withColumn(
        $form->image('Logo')->label('Firmenlogo'),
    )->withColumn(
        $form->image('Favicon'),
    )->withColumn(
        $form->color('PrimaryColor')->setPalette(['#FFFFFF', '#000000'])->label('Hauptfarbe'),
    ),
   $form->row()
   ->withColumn(
        $form->color('SecundaryColor')->setPalette(['#FFFFFF', '#000000'])->label('Sekundäre Farbe'),
   )->withColumn(
        $form->color('ThirdColor')->setPalette(['#FFFFFF', '#000000'])->label('Dritte Farbe'),
   )->withColumn(
        $form->color('FourthColor')->setPalette(['#FFFFFF', '#000000'])->label('Vierte Farbe'),
   ),
   $form->row()
    ->withColumn(
        $form->color('fifthColor')->setPalette(['#FFFFFF', '#000000'])->label('fünfte Farbe'),
    )->withColumn(
        $form->color('Back1Color')->setPalette(['#FFFFFF', '#000000'])->label('Hintergrundfarbe 1')        
    )->withColumn(
        $form->color('Back2Color')->setPalette(['#FFFFFF', '#000000'])->label('Hintergrundfarbe 2')
    )
]);
$button = [
    'primary' => '1',
    'secundary' => '2',
    'other' => '3',
];
// Contact
$contact = $form->fieldset('Contact Info', 'Contact Details and location(s)', [
    $form->text('Email')->label('E-mail'),
    $form->repeater('Branch')->setFields([
        $form->row(
            $form->text('Title')->label('Titel'),
            $form->text('Street')->label('Street'),
            $form->text('City')->label('City'),
        ),
        $form->row(
            $form->image('Map')->label('Maps'),
            $form->text('Cp')->label('Postal Code'),
            $form->text('Tel')->label('Phone'),
        ),
        $form->toggle('Buttons')->label('Button'),
        $form->row(
            $form->text('Button_text')->label('Button-Text'),
            $form->text('Button_Url')->label('Button-Url'),
            $form->select('Button_color')->label('Button-Farbe')->setOptions($button)->setDefault(1)
        )->when('buttons','=','1')
    ])->setLimit(2)
]);

// Contact
$header = $form->fieldset('Header', ' Header Settings', [
    $form->radio('HeaderFormat')->setOptions([
        'One' => [
            'src' => get_template_directory_uri().'/assets/components/headers/header_1.png',
            'value' => 1
        ],
        'Two' => [
            'src' => get_template_directory_uri().'/assets/components/headers/header_2.png',
            'value' => 2
        ],
        'Tree' => [
            'src' => get_template_directory_uri().'/assets/components/headers/header_3.png',
            'value' => 3
        ],
        'Four' => [
            'src' => get_template_directory_uri().'/assets/components/headers/header_4.png',
            'value' => 4
        ],
        'Five' => [
            'src' => get_template_directory_uri().'/assets/components/headers/header_5.png',
            'value' => 5
        ],
        'Six' => [
            'src' => get_template_directory_uri().'/assets/components/headers/header_6.png',
            'value' => 6
        ],
        'Sevent' => [
            'src' => get_template_directory_uri().'/assets/components/headers/header_7.png',
            'value' => 7
        ],
        'eight' => [
            'src' => get_template_directory_uri().'/assets/components/headers/header_8.png',
            'value' => 8
        ],
        'nine' => [
            'src' => get_template_directory_uri().'/assets/components/headers/header_9.png',
            'value' => 9
        ],
    ])->useImages()->setDefault(2)->setAttribute('class','test')->label('Header Format'),
    $form->toggle('Topbar')->label('Topbar'),
    $form->radio('TopbarFormat')->setOptions([
        'One' => [
            'src' => get_template_directory_uri().'/assets/components/topbar/topbar_1.png',
            'value' => 1
        ],
        'Two' => [
            'src' => get_template_directory_uri().'/assets/components/topbar/topbar_2.png',
            'value' => 2
        ],
    ])->useImages()->setDefault(2)->when('topbar')
]);
$footer = $form->fieldset('Footer', ' Footer Settings', [
    $form->radio('FooterFormat')->setOptions([
        'One' => [
            'src' => get_template_directory_uri().'/assets/components/footers/footer_1.png',
            'value' => 1
        ],
        'Two' => [
            'src' => get_template_directory_uri().'/assets/components/footers/footer_2.png',
            'value' => 2
        ],
        'Tree' => [
            'src' => get_template_directory_uri().'/assets/components/footers/footer_3.png',
            'value' => 3
        ],
    ])->useImages()->setDefault(2)->label('Footer Format'),
]);
$socket = $form->fieldset('Socket', ' Socket Settings', [
    $form->radio('SocketFormat')->setOptions([
        'One' => [
            'src' => get_template_directory_uri().'/assets/components/sockets/socket_1.png',
            'value' => 1
        ],
        'Two' => [
            'src' => get_template_directory_uri().'/assets/components/sockets/socket_2.png',
            'value' => 2
        ],
        'Tree' => [
            'src' => get_template_directory_uri().'/assets/components/sockets/socket_3.png',
            'value' => 3
        ],
        'four' => [
            'src' => get_template_directory_uri().'/assets/components/sockets/socket_4.png',
            'value' => 3
        ],
    ])->useImages()->setDefault(2)->label('Footer Format'),
]);

$options = [
    'Inside' => '1',
    'Outside' => '0',
];
$foo = [$footer,$socket];

$headerOptions = $form->fieldset('Header Options', 'Header Options', [
    $form->row(
        $form->color('Background Color ')->label('Background Color'),
        $form->select('grid')->label('Positioning: Inside grid or Outside grid')->setOptions($options)->setDefault(1),
        $form->select('topbarMenu')->label('Topbar Menu')->when('topbar')
    ),    
    $form->section(
        $form->row(
            $form->text('CTA Button Text')->label('CTA Button Text'),
            $form->text('CTA Button URL')->label('CTA Button URL'),
        )
    )->setAttribute('id','mp_header_selector'),
    $form->toggle('Sticky_Header')->label('Sticky Header'),
    $form->image('Sticky Header Logo')->label('Sticky Header Logo')->when('sticky_header'),
   
]);

$socialIcons = $form->fieldset('Social Icons', 'Social Icons', [
    $form->repeater('Icons')->setFields([
        $form->row(
            $form->image('Icon'),
            $form->text('URL')
        ),
    ])
]);

$selectfonts = ['Overpass-Regular'=>'Overpass-Regular','Overpass-Bold'=>'Overpass-Bold','Overpass-Italic'=>'Overpass-Italic','Overpass-Light'=>'Overpass-Light',
                'OpenSans-Regular'=>'OpenSans-Regular','OpenSans-Bold'=>'OpenSans-Bold','OpenSans-Italic'=>'OpenSans-Italic','OpenSans-Light'=>'Overpass-Light',
                'Roboto-Regular'=>'Roboto-Regular','Roboto-Bold'=>'Roboto-Bold','Roboto-Italic'=>'Roboto-Italic','Roboto-Light'=>'Roboto-Light',
                'Saira-Regular'=>'Saira-Regular','Saira-Bold'=>'Saira-Bold','Saira-Italic'=>'Saira-Italic','Saira-Light'=>'Saira-Light','MicrogrammaDBolExt'=>'MicrogrammaDBolExt',];

$fonts = $form->fieldset('Social Icons', 'Social Icons', [
    $form->row()
    ->withColumn(
        $form->fieldset('H1', 'H1 Styles', [
            $form->select('h1font')->label('Select Font')->setOptions($selectfonts),
            $form->input('H1font_size')->setType('number'),
        ]))
        ->withColumn(
        $form->fieldset('H2', 'H2 Styles', [
            $form->select('h2font')->label('Select Font')->setOptions($selectfonts),
            $form->input('H2font_size')->setType('number')->label('Font Size'),
        ]))
        ->withColumn(
        $form->fieldset('H3', 'H3 Styles', [
            $form->select('h3font')->label('Select Font')->setOptions($selectfonts),
            $form->input('H3font_size')->setType('number')->label('Font Size'),
        ])),    
    $form->row()
        ->withColumn(
        $form->fieldset('H4', 'H4 Styles', [
            $form->select('h4font')->label('Select Font')->setOptions($selectfonts),
            $form->input('H4font_size')->setType('number')->label('Font Size'),
        ]))
        ->withColumn(
        $form->fieldset('P', 'P Styles', [
            $form->select('pfont')->label('Select Font')->setOptions($selectfonts),
            $form->input('Pfont_size')->setType('number')->label('Font Size'),
        ]))
    
]);

$buttons = $form->fieldset('Buttons', 'Buttons', [
    $form->fieldset('Primary Button', 'Primary Button', [
        $form->toggle('Gradiant_Primary'),
        $form->section(
            $form->row(
                $form->color("PfirstColor")->label('First Color'),
                $form->color('PSecondColor')->label('Second Color'),
                $form->color('PTextColor')->label('Text Color'),
            ),
            $form->row(
                $form->color('PHoverColor')->label('Hover Color'),
                $form->input('PRadius')->setType('number')->label('Border Radius'),
                $form->input('PFont_Size')->setType('number')->label('Font Size'),
            )
        )->when('gradiant_primary','=',true),
        $form->section(
            $form->row(
                $form->color('PBackground'),
                $form->color('PBorder_Color'),
                $form->color('PHover_Color'),
            ),
            $form->row(
                $form->color('PTextColor2')->label('Text Color'),
                $form->color('PTextHoverColor')->label('Hover Text Color'),
                $form->input('PFont_Size2')->setType('number')->label('Font Size'),
            )
        )->when('gradiant_primary','=',false)

    ]),
    $form->fieldset('Secondary Button Secondary', 'Secondary Button', [
        $form->toggle('Gradiant_Secundary'),
        $form->section(
            $form->row(
                $form->color("SfirstColor")->label('First Color'),
                $form->color('SSecondColor')->label('Second Color'),
                $form->color('STextColor')->label('Text Color'),
            ),
            $form->row(
                $form->color('SHoverColor')->label('Hover Color'),
                $form->input('SRadius')->setType('number')->label('Border Radius'),
                $form->input('SFont_Size')->setType('number')->label('Font Size'),
            )
        )->when('gradiant_secundary','=',true),
        $form->section(
            $form->row(
                $form->color('SBackground'),
                $form->color('SBorder_Color'),
                $form->color('SHover_Color'),
            ),
            $form->row(
                $form->color('STextColor2')->label('Text Color'),
                $form->color('STextHoverColor')->label('Hover Text Color'),
                $form->input('SFont_Size2')->setType('number')->label('Font Size'),
            )            
        )->when('gradiant_secundary','=',false)        
    ]),
    $form->fieldset('Other Button', 'other Button', [
        $form->toggle('Gradiant_Other'),
        $form->section(
            $form->row(
                $form->color("OfirstColor")->label('First Color'),
                $form->color('OSecondColor')->label('Second Color'),
                $form->color('OTextColor')->label('Text Color'),
            ),
            $form->row(
                $form->color('OHoverColor')->label('Hover Color'),
                $form->input('ORadius')->setType('number')->label('Border Radius'),
                $form->input('OFont_Size')->setType('number')->label('Font Size'),
            )
        )->when('gradiant_other','=',true),
        $form->section(
            $form->row(
                $form->color('OBackground'),
                $form->color('OBorder_Color'),
                $form->color('OHover_Color'),
            ),
            $form->row(
                $form->color('OTextColor2')->label('Text Color'),
                $form->color('OTextHoverColor')->label('Hover Text Color'),
                $form->input('OFont_Size2')->setType('number')->label('Font Size'),
            )
        )->when('gradiant_other','=',false)
    ])

]);

// Maps
$help = '<a target="blank" href="https://developers.google.com/maps/documentation/javascript/get-api-key">Get Your Google Maps API</a>.';
$maps = $form->fieldset('Google Maps', 'Connection for Google Maps.', [
    $form->row(
        $form->text('Latitud')->label('Latitude'),
        $form->text('Longitud')->label('Longitude'),
        $form->text('GoogleApi')->label('Google API Key'),
    ),
    $help
]);

// Footer
function array_push_assoc($array, $key, $value){     $array[$key] = $value;     return $array; }
$menus = wp_get_nav_menus();
$options = [];
foreach($menus as $menu){
    $options = array_push_assoc($options, $menu->name, $menu->slug);
}

$footeroptions = $form->fieldset('Footer', 'All information in footer', [
    $form->row(
        $form->text('Copyright')->label('Copyright Information'),
        $form->select('Menü')->setOptions($options)->label('Footer Menu'),
        $form->color('Footer_bg_color')->label('Footer Backgroud Color')
    )
]);

// Save
$save = $form->submit( 'Save Changes' );

// Layout
$tabs = tr_tabs()->setFooter( $save )->layoutLeft();
$tabs->tab('General Info', 'dashboard', $general)->setDescription('Logo & Color Styles');
$tabs->tab('Header', 'dashicons-schedule', $header)->setDescription('Header Style');
$tabs->tab('Header Options', 'dashicons-schedule', $headerOptions)->setDescription('Header Options');
$tabs->tab('Social Icons', 'dashicons-rss', $socialIcons)->setDescription('Social Icons');
$tabs->tab('Fonts', 'dashicons-editor-textcolor', $fonts)->setDescription('Fonts Settings');
$tabs->tab('Buttons', 'dashicons-button', $buttons)->setDescription('Buttons Settings');
$tabs->tab('Contact Info', 'dashicons-format-status', $contact)->setDescription('Contact Info');
$tabs->tab('Google Maps', 'dashicons-location', $maps)->setDescription('Google Maps Settings');
$tabs->tab('Footer', 'dashicons-align-full-width', $foo)->setDescription('Footer Information');
$tabs->tab('Footer Options', 'dashicons-align-full-width', $footeroptions)->setDescription('Footer Information');
$tabs->render();

echo $form->close();
?>
