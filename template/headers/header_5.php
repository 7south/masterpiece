<!-- MENU -->
<div id="mySidebar" class="sidebar">
<?php 
    wp_nav_menu( array(
        'container'      => 'div',
        'menu_class'     => 'menu', // Do not fall back to first non-empty menu.
        'theme_location' => 'primary_menu',
        'fallback_cb'    => false, // Do not fall back to wp_page_menu(),
    ) );
?>
</div>

<!-- TOPBAR -->
<?php  require_once 'topbar/topbar_'.tr_option_field('tr_theme_options.topbarformat').'.php'; ?>

<header class='header5'>

    <?php if(tr_option_field('tr_theme_options.grid') == 1){ ?>
    <div class="container">
    <?php } ?>

        <div class="navigation">

            <div class="logo">
            <a href="/"><img src="<?php echo cleanSrc(tr_option_field('tr_theme_options.logo')); ?>" alt="logo-otec"></a>
            </div>

            <div id="menu">
                <span class='caret'><i class="fas fa-bars"></i></span>
                <ul class="menu-mobile">
                    <li class='button'><a href="<?php echo tr_option_field('tr_theme_options.cta_button_url'); ?>"><?php echo tr_option_field('tr_theme_options.cta_button_text'); ?></a></li>
                </ul>
            </div>

        </div>

    <?php if(tr_option_field('tr_theme_options.grid') == 1){ ?>
    </div>
    <?php } ?>
    
</header>