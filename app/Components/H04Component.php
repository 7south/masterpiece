<?php
namespace App\Components;

use TypeRocket\Template\Component;

class H04Component extends Component
{
    protected $title = 'Hero mitSeitentitel Komponente';

    /**
     * Admin Fields
     */
    public function fields()
    {
        $form = $this->form();

        $color = [
            'Hauptfarbe'=> 'principal_bg_primary',
            'Sekundäre Farbe'=> 'principal_bg_secondary',
            'Dritte Farbe'=> 'principal_bg_Third',
        ];
        echo $form->row(
            $form->image('Background_image')->label('Hintergrundbild'),
            $form->select('Box_Background')->label('Akzentfarbe ')->setOptions($color)->setDefault(1)
        );
    }

    /**
     * Render
     *
     * @var array $data component fields
     * @var array $info name, item_id, model, first_item, last_item, component_id, hash
     */
    public function render(array $data, array $info)
    {   
        include 'functions.php';
        ?>
        <div class="builder-content">
            <div class="container-fluid cover<?php data($data,'box_background') ?> " <?php background($data,'background_image') ?>>
                <div id="h04">
                    <div class="row">
                    <div class="center">
                            <h1><?php echo get_the_title() ?></h1>
                            <span><?php the_breadcrumb() ?></span>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}