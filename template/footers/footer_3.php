 <div class="container-footer container">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-3 footerImgIcon">
        <?php if ( is_active_sidebar( 'footer-1' ) ) { ?>
            <?php dynamic_sidebar('footer-1'); ?>
        <?php } ?>
        </div>
        <div class="col-12 col-sm-12 col-md-3 linksContact">
            <?php if ( is_active_sidebar( 'footer-2' ) ) { ?>
                <?php dynamic_sidebar('footer-2'); ?>
            <?php } ?>
        </div> 
        <div class="col-12 col-sm-12 col-md-3 linksContact">
            <?php if ( is_active_sidebar( 'footer-3' ) ) { ?>
                <?php dynamic_sidebar('footer-3'); ?>
            <?php } ?>
        </div> 
        <div class="col-12 col-sm-12 col-md-3 linksContact">
        <?php if ( is_active_sidebar( 'footer-4' ) ) { ?>
            <?php dynamic_sidebar('footer-4'); ?>
        <?php } ?>
    </div>
</div>