<?php
namespace App\Components;

use TypeRocket\Template\Component;

class C02Component extends Component
{
    protected $title = 'Boxed Call 2 Action Komponente';

    /**
     * Admin Fields
     */
    public function fields()
    {
        $form = $this->form();

        $backgroud_color = [
            'Hintergrundfarbe 1' => 'bg-primary',
            'Hintergrundfarbe 2' => 'bg-secondary',
        ];
        echo $form->row(
            $form->text('Main_Title')->label('Überschrift'),
            $form->text('SubTitle')->label('Untertitel')
        );
        echo $form->row(
            $form->text('Button_text')->label('Button-Text'),
            $form->text('Button_link')->label('Button-Link')
        );
        echo $form->row(
            $form->toggle('Padding_Above')->label('Abstand oben'),
            $form->toggle('Padding_Below')->label('Abstand unten'),
            $form->select('Box_Background')->label('Hintergrundfarbe')->setOptions($backgroud_color)->setDefault(1)
        );
    }

    /**
     * Render
     *
     * @var array $data component fields
     * @var array $info name, item_id, model, first_item, last_item, component_id, hash
     */
    public function render(array $data, array $info)
    {   
        include 'functions.php';
        ?>
        <div class="builder-content <?php if(isset($data['padding_above']) && $data['padding_above'] != 0){ echo 'pt'; } ?>  <?php if(isset($data['padding_below']) && $data['padding_below'] != 0){ echo 'pb'; } ?> <?php data($data,'box_background') ?>">
            <div class="container p-none">
                <div id="c02">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-6">
                            <h5><?php data($data,'main_title') ?></h5>
                            <p><?php data($data,'subtitle') ?></p>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6">
                            <a href="<?php data($data,'button_link') ?>" class="btn btn-primary"><?php data($data,'button_text') ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
    }
}