<?php require_once("../../../wp-load.php"); ?>
<?php header("Content-type: text/css"); ?>

header{ background:<?php echo tr_option_field('tr_theme_options.background_color'); ?>; }

<?php 
$font_families_declared = ['h1','h2','h3','h4','p'];
function set_font_family($tag, $family="Gill Sans Extrabold",$weight,$size, $style){
  if(tr_option_field('tr_theme_options.font_family_'.$tag) != "")
  {
    echo "$tag{
      font-family: $family;
      font-weight: $weight;
      font-size: $size; 
      font-style: $style\r\n}\r\n";
  }  
}

foreach($font_families_declared as $font){
  //echo tr_option_field('tr_theme_options.font_family_'.$font);
  set_font_family($font,
    tr_option_field('tr_theme_options.font_family_'.$font), 
    tr_option_field('tr_theme_options.font_weight_'.$font),
    tr_option_field('tr_theme_options.font_size_'.$font),
    tr_option_field('tr_theme_options.font_style_'.$font)
  );
}
echo "\n";
var_dump( tr_option_field('tr_theme_options'))  
?>