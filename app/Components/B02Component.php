<?php
namespace App\Components;

use TypeRocket\Template\Component;

class B02Component extends Component
{
    protected $title = 'Kleines Bild mit Text Komponente';

    /**
     * Admin Fields
     */
    public function fields()
    {
        $form = $this->form();
        $alignment2 = [
            'Links' => '1',
            'Rechts' => '2',
        ];
        $backgroud_color = [
            'Hintergrundfarbe 1' => 'bg-primary',
            'Hintergrundfarbe 2' => 'bg-secondary',
        ];
        echo $form->image('Image')->label('Bild');
        echo $form->row(
            $form->text('Pre_Title')->label('Titel'),
            $form->text('Main_Title')->label('Überschrift'),
        );
        echo $form->row(
            $form->select('Alignment')->label('Ausrichtung')->setOptions($alignment2)->setDefault(1),
            $form->select('Box_Background')->label('Hintergrundfarbe')->setOptions($backgroud_color)->setDefault(1),
        );
        echo $form->editor('Description')->label('Beschreibung');
        echo $form->row(
            $form->toggle('Padding_Above')->label('Abstand oben'),
            $form->toggle('Padding_Below')->label('Abstand unten'),
        );
    }

    /**
     * Render
     *
     * @var array $data component fields
     * @var array $info name, item_id, model, first_item, last_item, component_id, hash
     */
    public function render(array $data, array $info)
    {   
        include 'functions.php';
        $align = $data['alignment'];
        ?>
        <div class="builder-content <?php if(isset($data['padding_above']) && $data['padding_above'] != 0){ echo 'pt'; } ?>  <?php if(isset($data['padding_below']) && $data['padding_below'] != 0){ echo 'pb'; } ?> <?php data($data,'box_background') ?>">
            <div id="b02">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-4 <?php if($align == "2"){ echo "order-md-2"; } ?>">
                            <img class="img-fluid" src="<?php echo cleanSrc($data['image']) ?>" alt="">
                        </div>
                        <div class="col-12 col-sm-12 col-md-8 <?php if($align == "2"){ echo "order-md-1"; } ?>">
                            <label><?php data($data,'pre_title') ?></label>
                            <h2><?php data($data,'main_title') ?></h2>
                            <p>
                            <?php data($data,'description') ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}