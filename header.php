<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" type="image/png" href="<?php echo cleanSrc(tr_option_field('tr_theme_options.favicon')) ?>">
	<title><?php echo wp_get_document_title(); ?></title>
	<?php wp_head(); ?>
	<script src="https://kit.fontawesome.com/0c40e92b5d.js" crossorigin="anonymous"></script>
</head>
<body class='<?php body_class(); ?>'>
<style type="text/css">
        :root {
          --primary-color: <?php echo tr_option_field('tr_theme_options.primarycolor'); ?>;
          --secundary-color: <?php echo tr_option_field('tr_theme_options.secundarycolor'); ?>;
		  --third-color: <?php echo tr_option_field('tr_theme_options.thirdcolor'); ?>;
		  --h1-font:<?php echo tr_option_field('tr_theme_options.h1font'); ?>;
		  --h2-font:<?php echo tr_option_field('tr_theme_options.h2font'); ?>;
		  --h3-font:<?php echo tr_option_field('tr_theme_options.h3font'); ?>;
		  --h4-font:<?php echo tr_option_field('tr_theme_options.h4font'); ?>;
		  --p-font:<?php echo tr_option_field('tr_theme_options.pfont'); ?>;
		  --h1-size:<?php echo tr_option_field('tr_theme_options.h1font_size'); ?>px;
		  --h2-size:<?php echo tr_option_field('tr_theme_options.h2font_size'); ?>px;
		  --h3-size:<?php echo tr_option_field('tr_theme_options.h3font_size'); ?>px;
		  --h4-size:<?php echo tr_option_field('tr_theme_options.h4font_size'); ?>px;
		  --p-size:<?php echo tr_option_field('tr_theme_options.pfont_size'); ?>px;
		  --footer-bg-color: <?php echo tr_option_field('tr_theme_options.footer_bg_color'); ?>;
		  --background-color1: <?php echo tr_option_field('tr_theme_options.back1color'); ?>;
		  --background-color2: <?php echo tr_option_field('tr_theme_options.back2color'); ?>;
        }
		.btn-primary{
			background: <?php echo tr_option_field('tr_theme_options.secondcolor'); ?>;
			background: -moz-linear-gradient(150deg, <?php echo tr_option_field('tr_theme_options.secondcolor'); ?> 0%, <?php echo tr_option_field('tr_theme_options.firstcolor'); ?> 100%);
			background: -webkit-linear-gradient(150deg, <?php echo tr_option_field('tr_theme_options.secondcolor'); ?> 0%, <?php echo tr_option_field('tr_theme_options.firstcolor'); ?> 100%);
			background: linear-gradient(150deg, <?php echo tr_option_field('tr_theme_options.secondcolor'); ?> 0%, <?php echo tr_option_field('tr_theme_options.firstcolor'); ?> 100%);
			filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="<?php echo tr_option_field('tr_theme_options.secondcolor'); ?>", endColorstr="<?php echo tr_option_field('tr_theme_options.firstcolor'); ?>", GradientType=1);
			padding: 12px 35px;
			border-radius: <?php echo tr_option_field('tr_theme_options.radius'); ?>px;
			text-transform: uppercase;
			color: <?php echo tr_option_field('tr_theme_options.textcolor'); ?>;
			font-family: <?php echo tr_option_field('tr_theme_options.pfont'); ?>;
			font-size: <?php echo tr_option_field('tr_theme_options.font_size'); ?>px;
			letter-spacing: 2px;	
		}
		.btn-secondary{			
			text-transform: uppercase;
			color: <?php echo tr_option_field('tr_theme_options.stextcolor'); ?>;
			font-family: "roboto";
			font-weight: 500;
			color: #7a7a7a;
			font-size: <?php echo tr_option_field('tr_theme_options.sfont_size'); ?>px;
		}
    </style>
<?php
 //require_once 'template/topbar/topbar_'.tr_option_field('tr_theme_options.topbarformat').'.php';
 require_once 'template/headers/header_'.tr_option_field('tr_theme_options.headerformat').'.php';
