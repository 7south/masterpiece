<?php get_header();

if( tr_post_field("use_builder") == '1') {
    tr_components_field('builder');
} else {
    get_template_part('standard');
}

get_footer();