<?php
namespace App\Components;

use TypeRocket\Template\Component;

class H02Component extends Component
{
    protected $title = 'Hero mit großem Titel Komponente';

    /**
     * Admin Fields
     */
    public function fields()
    {
        $form = $this->form();

        $color = [
            'Hauptfarbe'=> 'principal_bg_primary',
            'Sekundäre Farbe'=> 'principal_bg_secondary',
            'Dritte Farbe'=> 'principal_bg_Third',
        ];
        echo $form->row(
            $form->image('Hero_Image')->label('Titelbild'),
            $form->text('Text_Title')->label('Überschrift'),
        );
        echo $form->row(
            $form->text('SubTitle')->label('Untertitel'),
            $form->select('Box_Background')->label('Hintergrundfarbe der Box')->setOptions($color)->setDefault(1)
        );
    }

    /**
     * Render
     *
     * @var array $data component fields
     * @var array $info name, item_id, model, first_item, last_item, component_id, hash
     */
    public function render(array $data, array $info)
    {   
        include 'functions.php';
        ?>
        <div class="builder-content">
            <div class="container-fluid">
                <div id="h02">
                    <div class="row">
                        <div class="img-holder">
                            <img class="img-fluid" src="<?php echo cleanSrc($data['hero_image']) ?>" alt="o-tec">
                        </div>
                    </div>
                    <div class="container">
                        <div class="row floatDiv">
                            <div class="col-12 col-sm-12 col-md-7 <?php data($data,'box_background') ?>">
                                <h2><?php data($data,'text_title') ?></h2>
                                <p><?php data($data,'subtitle') ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}