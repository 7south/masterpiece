<div class="container-footer container">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-4 footerImg">
            <img src="https://masterpiece.ddev.site/wp-content/uploads/2021/03/masterpiece-e1616453372554.png" alt="">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum in arcu est. Vivamus pellentesque magna eu imperdiet laoreet. Aliquam sed sodales neque. Vivamus vehicula tempor orci in aliquam. Praesent a tempor enim, at aliquam purus. Sed hendrerit gravida consequat. </p>
        </div>
        <div class="col-12 col-sm-12 col-md-2"></div> 
        <div class="col-12 col-sm-12 col-md-6">
            <ul class="menu">
                <li class="active">ABOUT US</li>
                <li>SERVICE</li>
                <li>PARTNERS</li>
                <li>CONTACT US</li>
            </ul>
            <div class="icons">
                <a href="#"><div alt="f12d" class="dashicons dashicons-instagram"></div></a>
                <a href=""><div alt="f12d" class="dashicons dashicons-twitter"></div></a>
                <a href=""><div alt="f12d" class="dashicons dashicons-facebook"></div></a>
            </div>
        </div> 
    </div>
</div>