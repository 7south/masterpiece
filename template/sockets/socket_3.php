<div class="page-socket" id="socket3">
    <div class="container-socket container">
        <div class="row hr-socket">
            <div class="col-12 col-sm-12 col-md-6">
                <p><?php echo tr_option_field('tr_theme_options.copyright'); ?></p>
            </div>
            <div class="col-12 col-sm-12 col-md-6 rightSocket">
                <div class="iconsSocket">
                    <a href="#"><div alt="f12d" class="dashicons dashicons-instagram"></div></a>
                    <a href=""><div alt="f12d" class="dashicons dashicons-twitter"></div></a>
                    <a href=""><div alt="f12d" class="dashicons dashicons-facebook"></div></a>
                </div>
            </div>
        </div>
    </div>
</div>