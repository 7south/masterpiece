<?php
namespace App\Components;

use TypeRocket\Template\Component;

class L02Component extends Component
{
    protected $title = 'Icon Boxes Komponente';

    /**
     * Admin Fields
     */
    public function fields()
    {
        $form = $this->form();

        $alignment3 = [
            'Oben' => '1',
            'Links' => '2',
            'Zentriert' => '3',
        ];
        $button = [
            'Primary' => 'btn-primary',
            'Secondary' => 'btn-secondary',
        ];
        $color = [
            'Hauptfarbe'=> 'principal_bg_primary',
            'Sekundäre Farbe'=> 'principal_bg_secondary',
            'Dritte Farbe'=> 'principal_bg_Third',
        ];
        $backgroud_color = [
            'Hintergrundfarbe 1' => 'bg-primary',
            'Hintergrundfarbe 2' => 'bg-secondary',
        ];
        
        echo $form->row(
            $form->select('Icon_orientation')->label('Icon-Ausrichtung Oben, Links oder Zentriert')->setOptions($alignment3)->setDefault(1)
        );
        $repeater = $form->repeater('Link_Box')->setFields([
            $form->row(
                $form->text('Box_Image')->label('Icon Code'),
                $form->text('Box_Title')->label('Überschrift'),
            ),           
            $form->editor('Box_Description')->label('Kurztext'),
            $form->toggle('Actv_B')->label('Button aktiviern'),
            $form->row(
                $form->text('Button_Text')->label('Button-Text'),
                $form->text('Button_Url')->label('Button-Url'),
                $form->select('Button_color')->label('Button-Typ')->setOptions($button)->setDefault(1),
            )->when('actv_b','=','1'),            
        ]);
        echo $repeater;
        echo $form->row(
            $form->select('Box_hover_color')->label('Farbe der Box bei Hover')->setOptions($color)->setDefault(1),
            $form->select('Box_Background')->label('Hintergrundfarbe')->setOptions($backgroud_color)->setDefault(1),
            $form->toggle('Padding_Above')->label('Abstand oben'),
            $form->toggle('Padding_Below')->label('Abstand unten')
        );
    }

    /**
     * Render
     *
     * @var array $data component fields
     * @var array $info name, item_id, model, first_item, last_item, component_id, hash
     */
    public function render(array $data, array $info)
    {   
        include 'functions.php';
        ?>
        <div class="builder-content <?php if(isset($data['padding_above']) && $data['padding_above'] != 0){ echo 'pt'; } ?>  <?php if(isset($data['padding_below']) && $data['padding_below'] != 0){ echo 'pb'; } ?> <?php data($data,'box_background') ?>">
            <div class="container">
                <div id="l02">
                <?php if($data['icon_orientation'] == '1'){?>
                    <div class="row">
                        <?php 
                        if(is_array($data['link_box'])){
                        foreach($data['link_box'] as $icon){ ?>
                        <div class="col-12 col-sm-12 col-md-3 top green">
                            
                            <?php if(isset($icon['box_image'])){ ?><i class="<?php echo $icon['box_image']; ?> dashicons"></i><?php } ?>
                            <h3><?php data($icon,'box_title') ?></h3>
                            <p><?php data($icon,'box_description') ?></p>
                            <?php if(isset($icon['actv_b']) && $icon['actv_b'] == '1'){ ?>
                                <a class="<?php data($icon,'button_color') ?>" href="<?php data($icon,'button_url') ?>"><?php data($icon,'button_text') ?></a>
                            <?php } ?>
                        </div>
                        <?php }
                        }?>
                    </div>
                    <?php }
                     if($data['icon_orientation'] == '2'){?>
                    <div class="row">
                        <?php 
                        if(is_array($data['link_box'])){
                        foreach($data['link_box'] as $icon){ ?>
                        <div class="col-12 col sm-12 col-md-4 side green">
                            
                        <?php if(isset($icon['box_image'])){ ?><i class="<?php echo $icon['box_image']; ?> dashicons"></i><?php } ?>
                            <div class="text">
                                <h3><?php data($icon,'box_title') ?></h3>
                                <p><?php data($icon,'box_description') ?></p>
                                <?php if(isset($icon['actv_b']) && $icon['actv_b'] == '1'){ ?>
                                    <a class="<?php data($icon,'button_color') ?>" href="<?php data($icon,'button_url') ?>"><?php data($icon,'button_text') ?></a>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } 
                        }?>
                    </div>
                    <?php 
                    }
                    if($data['icon_orientation'] == '3'){
                    ?>
                    <div class="row">
                    <?php 
                        if(is_array($data['link_box'])){
                        foreach($data['link_box'] as $icon){ ?>
                        <div class="col-12 col-sm-12 col-md-4 top-center green">
                             
                            <?php if(isset($icon['box_image'])){ ?><i class="<?php echo $icon['box_image']; ?> dashicons"></i><?php } ?>
                            <h3><?php data($icon,'box_title') ?></h3>
                            <p><?php data($icon,'box_description') ?></p>
                            <?php if(isset($icon['actv_b']) && $icon['actv_b'] == '1'){ ?>
                                <a class="<?php data($icon,'button_color') ?>" href="<?php data($icon,'button_url') ?>"><?php data($icon,'button_text') ?></a>
                            <?php } ?>
                        </div>
                        <?php }
                        } ?>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php
    }
}