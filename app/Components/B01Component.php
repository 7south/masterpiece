<?php
namespace App\Components;

use TypeRocket\Template\Component;

class B01Component extends Component
{
    protected $title = 'Bild mit Text Komponente';

    /**
     * Admin Fields
     */
    public function fields()
    {
        $form = $this->form();

        $backgroud_color = [
            'Hintergrundfarbe 1' => 'bg-primary',
            'Hintergrundfarbe 2' => 'bg-secondary',
        ];
        $alignment2 = [
            'Links' => '1',
            'Recht' => '2',
        ];
        $button = [
            'Primary' => 'btn-primary',
            'Secondary' => 'btn-secondary',
        ];
        echo $form->image('Image')->label('Bild');
        echo $form->editor('Description')->label('Beschreibung');
        echo $form->toggle('Button')->label('Button aktiviern');
        echo $form->row(
            $form->text('Button_text')->label('Button-Text'),
            $form->text('Button_link')->label('Button-Url'),
            $form->select('Button_color')->label('Button-Farbe')->setOptions($button)->setDefault(1),
        )->when('button','=','1');
        echo $form->row(
            $form->select('Alignment')->label('Ausrichtung')->setOptions($alignment2)->setDefault(1),
            $form->select('Box_Background')->label('Hintergrundfarbe')->setOptions($backgroud_color)->setDefault(1),
        );
        echo $form->row(
            $form->toggle('Padding_Above')->label('Abstand oben'),
            $form->toggle('Padding_Below')->label('Abstand unten'),
        );
    }

    /**
     * Render
     *
     * @var array $data component fields
     * @var array $info name, item_id, model, first_item, last_item, component_id, hash
     */
    public function render(array $data, array $info)
    {   
        include 'functions.php';
        ?>
        <div class="builder-content <?php if(isset($data['padding_above']) && $data['padding_above'] != 0){ echo 'pt'; } ?>  <?php if(isset($data['padding_below']) && $data['padding_below'] != 0){ echo 'pb'; } ?> <?php data($data,'box_background') ?>">
            <div id="b01">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-6">
                            <img class="img-fluid" src="<?php echo cleanSrc($data['image']) ?>" alt="">
                        </div>
                        <div class="col-12 col-sm-12 col-md-6">
                            <p>
                                <p><?php data($data,'description') ?></p>
                            </p>
                            <?php if(isset($data['button']) && $data['button'] == '1'){ ?>
                            <a class="btn <?php data($data,'button_color') ?>" href="<?php data($data,'button_link') ?>"><?php data($data,'button_text') ?></a>
                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}