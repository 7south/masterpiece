<!-- TOPBAR -->
<?php  require_once 'topbar/topbar_'.tr_option_field('tr_theme_options.topbarformat').'.php'; ?>

<header class='header8'>

    <?php if(tr_option_field('tr_theme_options.grid') == 1){ ?>
    <div class="container">
    <?php } ?>

        <div class="navigation">
            <div id="menu">
            <?php 
                wp_nav_menu( array(
                    'menu_class'     => 'menu', // Do not fall back to first non-empty menu.
                    'theme_location' => 'primary_menu',
                    'fallback_cb'    => false, // Do not fall back to wp_page_menu(),
                ) );
            ?>
                <div class="logo">
                <a href="/"><img src="<?php echo cleanSrc(tr_option_field('tr_theme_options.logo')); ?>" alt="logo-otec"></a>
                </div>
                <?php 
                wp_nav_menu( array(
                    'menu_class'     => 'menu', // Do not fall back to first non-empty menu.
                    'theme_location' => 'primary_menu',
                    'fallback_cb'    => false, // Do not fall back to wp_page_menu(),
                ) );
            ?>
            </div>

        </div>

    <?php if(tr_option_field('tr_theme_options.grid') == 1){ ?>
    </div>
    <?php } ?>
    
</header>