<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Assets
    |--------------------------------------------------------------------------
    |
    | The URL where TypeRocket assets are found.
    |
    */
    'assets' => \TypeRocket\Utility\Helper::assetsUrlBuild(),

    /*
    |--------------------------------------------------------------------------
    | Components
    |--------------------------------------------------------------------------
    |
    | The URL where TypeRocket component assets are found.
    |
    */
    'components' => get_template_directory_uri() . '/assets/components',

    /*
    |--------------------------------------------------------------------------
    | Typerocket Assets
    |--------------------------------------------------------------------------
    |
    | The URL where TypeRocket Core assets are found.
    |
    */
    'typerocket' => \TypeRocket\Utility\Helper::assetsUrlBuild( '/typerocket' ),
];