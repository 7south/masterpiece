<?php
namespace App\Components;

use TypeRocket\Template\Component;

class H03Component extends Component
{
    protected $title = 'Hero mit Bild-Slider Komponente';

    /**
     * Admin Fields
     */
    public function fields()
    {
        $form = $this->form();

        echo $form->gallery('slider')->label('Hero mit Slider');
    }

    /**
     * Render
     *
     * @var array $data component fields
     * @var array $info name, item_id, model, first_item, last_item, component_id, hash
     */
    public function render(array $data, array $info)
    {   
        include 'functions.php';
        ?>
<div class="builder-content">
    <div class="container-fluid">
        <div id="h03">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 p-none">
                    <div id="sliderh03" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#sliderh03" data-slide-to="0" class="active"></li>
                            <li data-target="#sliderh03" data-slide-to="1"></li>
                            <li data-target="#sliderh03" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                        <?php foreach($data['slider'] as $slider){ ?>
                            <div class="carousel-item active">
                                <div class="img-holder">
                                    <img class="img-fluid" src="<?php echo cleanSrc($slider) ?>" alt="First slide">
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <a class="carousel-control-prev" href="#sliderh03" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#sliderh03" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        <?php
    }
}