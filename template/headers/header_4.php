<!-- MENU -->
<div id="mySidebar" class="sidebar">
<?php 
    wp_nav_menu( array(
        'container'      => 'div',
        'menu_class'     => 'menu', // Do not fall back to first non-empty menu.
        'theme_location' => 'primary_menu',
        'fallback_cb'    => false, // Do not fall back to wp_page_menu(),
    ) );
?>
</div>
<!-- TOPBAR -->
<?php  require_once 'topbar/topbar_'.tr_option_field('tr_theme_options.topbarformat').'.php'; ?>
<header class='header4'>

    <?php if(tr_option_field('tr_theme_options.grid') == 1){ ?>
    <div class="container">
    <?php } ?>

        <div class="navigation">

            <div class="logo">
            <a href="/"><img src="<?php echo cleanSrc(tr_option_field('tr_theme_options.logo')); ?>" alt="logo-otec"></a>
            </div>
            <div id="menu">
                <span class='caret'><i class="fas fa-bars"></i></span>
                <ul class="menu-mobile">
                <div class="social">
                    <?php if(is_array(tr_option_field('tr_theme_options.icons'))) {?>
                    <?php foreach( tr_option_field('tr_theme_options.icons') as $icon){ ?>
                        <li><a href="<?php echo $icon['url'] ?>"><img src="<?php echo cleanSrc($icon['icon']) ?>" alt=""></a></li>
                    <?php } 
                    }?>
                </div>
                </ul>
            </div>

        </div>

    <?php if(tr_option_field('tr_theme_options.grid') == 1){ ?>
    </div>
    <?php } ?>
    
</header>