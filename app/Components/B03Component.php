<?php
namespace App\Components;

use TypeRocket\Template\Component;

class B03Component extends Component
{
    protected $title = 'Text mit Icon Box Komponente';

    /**
     * Admin Fields
     */
    public function fields()
    {
        $form = $this->form();

        $alignment2 = [
            'links' => '1',
            'Recht' => '2',
        ];
        $backgroud_color = [
            'Hintergrundfarbe 1' => 'bg-primary',
            'Hintergrundfarbe 2' => 'bg-secondary',
        ];
        echo $form->row(
            $form->text('PreTitle')->label('Titel'),
            $form->text('Main_Title')->label('Überschrift'),
        );
        echo $form->editor('Description')->label('Beschreibung');
        echo $form->row(
            $form->image('Icon')->label('Icon/Bild'),
            $form->text('Icon_Title')->label('Icon-Überschrift'),
        );
        echo $form->editor('Icon_Description')->label('Kurzbeschreibung');
        echo $form->row(
            $form->text('Button_text')->label('Button-Text'),
            $form->text('Button_link')->label('Button-Link')
        );
        echo $form->row(
            $form->select('Alignment')->label('Ausrichtung')->setOptions($alignment2)->setDefault(1),
            $form->select('Box_Background')->label('Hintergrundfarbe')->setOptions($backgroud_color)->setDefault(1)
        );
        echo $form->row(
            $form->toggle('Padding_Above')->label('Abstand oben'),
            $form->toggle('Padding_Below')->label('Abstand unten')
        );
    }

    /**
     * Render
     *
     * @var array $data component fields
     * @var array $info name, item_id, model, first_item, last_item, component_id, hash
     */
    public function render(array $data, array $info)
    {   
        include 'functions.php';
        ?>
        <div class="builder-content <?php if(isset($data['padding_above']) && $data['padding_above'] != 0){ echo 'pt'; } ?>  <?php if(isset($data['padding_below']) && $data['padding_below'] != 0){ echo 'pb'; } ?> <?php data($data,'box_background') ?>">
            <div class="container">
                <div id="b03">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-9">
                            <span><?php data($data,'pretitle') ?></span>
                            <h2><?php data($data,'main_title') ?></h2>
                            <p>
                            <?php data($data,'description') ?>
                            </p>
                        </div>
                        <div class="col-12 col-sm-12 col-md-3 icon">
                            <span class="dashicons dashicons-saved"></span>
                            <span class="subtitle"><?php data($data,'icon_title') ?></span>
                            <p><?php data($data,'icon_description') ?></p>
                            <a href="<?php data($data,'button_link') ?>"><?php data($data,'button_text') ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}