<?php
namespace App\Components;

use TypeRocket\Template\Component;

class C01Component extends Component
{
    protected $title = 'Volle Breite Call 2 Action Komponente';

    /**
     * Admin Fields
     */
    public function fields()
    {
        $form = $this->form();

        $color = [
            'Hauptfarbe'=> 'principal_bg_primary',
            'Sekundäre Farbe'=> 'principal_bg_secondary',
            'Dritte Farbe'=> 'principal_bg_Third',
        ];
        echo $form->row(
            $form->text('Main-Title')->label('Überschrift'),
            $form->text('SubTitle')->label('Untertitel')
        );
        echo $form->row(
            $form->text('Button_text')->label('Button-Text'),
            $form->text('Button_link')->label('Button-url')
        );
        echo $form->row(
            $form->select('Box_Background')->label('Hintergrundfarbe')->setOptions($color)->setDefault(1),
            $form->image('Background_Image')->label('Hintergundbild')
        );
    }

    /**
     * Render
     *
     * @var array $data component fields
     * @var array $info name, item_id, model, first_item, last_item, component_id, hash
     */
    public function render(array $data, array $info)
    {   
        include 'functions.php';
        ?>
        <div class="builder-content <?php data($data,'box_background') ?>">
            <div class="container-fluid p-none cover" <?php background($data,'background_image') ?>>
                <div id="c01">
                    <div class="row m-none">
                        <div class="center">
                            <h2><?php data($data,'main_title') ?></h2>
                            <p><?php data($data,'subtitle') ?></p>
                            <a class="btn btn-degrad btn-sm" href="<?php data($data,'button_link') ?>"><?php data($data,'button_text') ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}