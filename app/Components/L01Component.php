<?php
namespace App\Components;

use TypeRocket\Template\Component;

class L01Component extends Component
{
    protected $title = 'Link mit Bild in Karten Komponente';

    /**
     * Admin Fields
     */
    public function fields()
    {
        $form = $this->form();

        $backgroud_color = [
            'Hintergrundfarbe 1' => 'bg-primary',
            'Hintergrundfarbe 2' => 'bg-secondary',
        ];
        $button = [
            'Primary' => 'btn-primary',
            'Secondary' => 'btn-secondary',
        ];
        
        $color = [
            'Hauptfarbe'=> 'principal_bg_primary',
            'Sekundäre Farbe'=> 'principal_bg_secondary',
            'Dritte Farbe'=> 'principal_bg_Third',
        ];
        $repeater = $form->repeater('Link_Box')->setFields([
            $form->row(
                $form->image('Box_Image')->label('Bild'),
                $form->text('Box_Title')->label('Überschrift'),
            ),           
            $form->editor('Box_Description')->label('Kurztext'),
            $form->row(
                $form->text('Button_Text')->label('Button-Text'),
                $form->text('Button_Url')->label('Button-Url'),
                $form->select('Button_color')->label('Button-Typ')->setOptions($button)->setDefault(1),
            ),            
        ]);
        echo $repeater;
        echo $form->row(
            $form->select('Box_hover_color')->label('Farbe der Box bei Hover')->setOptions($color)->setDefault(1),
            $form->select('Box_Background')->label('Hintergrundfarbe')->setOptions($backgroud_color)->setDefault(1),
            $form->toggle('Padding_Above')->label('Abstand oben'),
            $form->toggle('Padding_Below')->label('Abstand unten')
        );
    }

    /**
     * Render
     *
     * @var array $data component fields
     * @var array $info name, item_id, model, first_item, last_item, component_id, hash
     */
    public function render(array $data, array $info)
    {   
        include 'functions.php';
        ?>
        <div class="builder-content <?php if(isset($data['padding_above']) && $data['padding_above'] != 0){ echo 'pt'; } ?>  <?php if(isset($data['padding_below']) && $data['padding_below'] != 0){ echo 'pb'; } ?> <?php data($data,'box_background') ?>">
            <div class="container"> 
                <div id="l01">
                    <div class="row">
                        <?php 
                        if(is_array($data['link_box'])){
                        foreach($data['link_box'] as $icon) {?>
                        <div class="col-12 col-sm-12 col-md-4 green-card">
                            <div class="card-top">
                            <a href="<?php data($icon,'button_url') ?>"><div class="container-img cover" <?php background($icon,'box_image') ?>>
                                    <div class="tag"></div>
                                    <span class="dashicons dashicons-external"></span>
                                </div>
                            </div></a>
                            <div class="card-bottom">
                                <div class="card-body">
                                    <h2 class="card-title"><?php data($icon,'box_title') ?></h2>
                                    <p class="card-text"><?php data($icon,'box_description') ?></p>
                                    <a href="<?php data($icon,'button_url') ?>" class="btn <?php data($icon,'button_color') ?>"><?php data($icon,'button_text') ?></a>
                                </div>
                            </div>
                        </div>
                        <?php }
                        }?>
                    </div>
                </div>
            </div>
        </div>
     
        <!-- <div class="container slick-slider">
            <div id="l01" class="slider-master slick-slider">               
                    <?php
                       foreach($data['link_box'] as $icon){ 
                        
                        ?>
                        <div class="blue">
                            <div class="card-bottom-slider">
                                <div class="card-body">
                                    <h2 class="card-title">Card title</h2>
                                    <a href="#" class="btn btn-primary btn-outline-light">Go somewhere</a>
                                </div>
                            </div>
                            <div class="card-top-slider">
                                <div class="container-img cover"
                                    style="background-image: url('https://o-tec.ddev.site/wp-content/uploads/2021/03/107628334-otec-online-X1DHAIVHaea_jpg_2.png')">
                                    <div class="tag"></div>
                                    <span class="dashicons dashicons-external"></span>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
            </div>
        </div> -->
        <?php
    }
}