<div class="page-socket" id="socket2">
    <div class="container-socket container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-6">
                <p><?php echo tr_option_field('tr_theme_options.copyright'); ?></p>
            </div>
            <div class="col-12 col-sm-12 col-md-6 rightSocket">
                <p>A project made with ❤</p>
            </div>
        </div>
    </div>
</div>